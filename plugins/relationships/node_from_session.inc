<?php


/**
 * @file
 *
 *
 * We take a session variable, look in it for what we need to make a relcontext, and make it.
 * In the real world, this might be getting a taxonomy id from a node, for example.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Node from session"),
  'description' => t('Adds a node from existing session.'),
  'keyword' => 'node',
  'required context' => new ctools_context_required(t('Session context'), 'sessioncontext'),
  'context' => 'ctools_node_from_session_context',
  'settings form' => 'ctools_node_from_session_settings_form',
);

/**
 * Return a new context based on an existing context.
 */
function ctools_node_from_session_context($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node', NULL);
  }
  $node = node_load($context->data);
  // Send it to ctools.
  return ctools_context_create('node', $node);
}


