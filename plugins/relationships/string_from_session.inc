<?php

/**
 * @file
 * We take a session variable, look in it for what we need to make a relcontext, and make it.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('String from session'),
  'description' => t('Get a seesion value as a string.'),
  'keyword' => 'string',
  'required context' => new ctools_context_required(t('Session context'), 'sessioncontext'),
  'context' => 'ctools_string_from_session_context',
);

/**
 * Return a new context based on an existing context.
 */
function ctools_string_from_session_context($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('string', NULL);
  }

  // Send it to ctools.
  return ctools_context_create('string', (string) $context->data);
}

