<?php
/**
 * @file
 * This CTools plugin grabs a session value
 * and lets you use it as a context in panels.
 */

$plugin = array(
  'title' => t("Session context"),
  'description' => t('grabs a session value and ads it as a ctools context'),
  'context' => 'ctools_session_context_context_create_sessioncontext',
  'context name' => 'sessioncontext',
  'keyword' => 'sessioncontext',
  'edit form' => 'ctools_session_context_settings_form',
  'convert list' => 'ctools_session_context_convert_list',
  'convert' => 'ctools_session_context_convert',
);

/**
 * Create a context, either from manual configuration (form).
 *
 * @param bool $empty
 *   If true, just return an empty context.
 * @param array $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param bool $conf
 *   TRUE if the $data is coming from admin configuration.
 *   FALSE if it's from a URL arg.
 *
 * @return array
 *   a Context object.
 */
function ctools_session_context_context_create_sessioncontext($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('sessioncontext');
  $context->plugin = 'sessioncontext';
  if ($empty) {
    var_dump('empty');
    return $context;
  }
  // Grab the session value if the session exist.
  if (isset($_SESSION['contextofchaos'][$data['session_name']])) {
    $session_context = $_SESSION['contextofchaos'][$data['session_name']];

    if (!empty($data) && $session_context) {
      $context->data = $session_context;
      $context->title = t("Session context");
      return $context;
    }
  }

  // If session don't exist in browser, send back fallback value.
  $context->data = $data['fallback_value'];
  $context->title = t("Session context");
  return $context;
}

/**
 * Form builder; settings for the context.
 */
function ctools_session_context_settings_form($form, &$form_state) {
  $form['session_name'] = array(
    '#title' => t('Session name'),
    '#description' => t('Enter the name of the session'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  if (isset($form_state['conf']['session_name'])) {
    $form['session_name']['#default_value'] = $form_state['conf']['session_name'];
  }

  $form['fallback_value'] = array(
    '#title' => t('Fallback value'),
    '#description' => t("Enter a value that must be returned if the above specified session don't exist ."),
    '#type' => 'textfield',
  );
  if (!empty($form_state['conf']['fallback_value'])) {
    $form['fallback_value']['#default_value'] = $form_state['conf']['fallback_value'];
  }
  return $form;
}

/**
 * Submit handler; settings form for the context.
 */
function ctools_session_context_settings_form_submit($form, &$form_state) {
  $form_state['conf']['session_name'] = $form_state['values']['session_name'];
  $form_state['conf']['fallback_value'] = $form_state['values']['fallback_value'];
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function ctools_session_context_convert_list() {
  return array(
    'session_value' => t('session value'),
  );
}

/**
 * Convert a context into a string to be used as a keyword by content types.
 */
function ctools_session_context_convert($context, $type) {
  switch ($type) {
    case 'session_value':
      return $context->data;
  }
}
